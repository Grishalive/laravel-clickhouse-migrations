<?php

namespace Grishalive\ClickhouseMigrations\Migrations;

interface MigrationInterface
{
    
    public function up();
    
    public function down();
    
}

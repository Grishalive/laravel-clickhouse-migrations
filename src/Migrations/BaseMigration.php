<?php

namespace Grishalive\ClickhouseMigrations\Migrations;

abstract class BaseMigration implements MigrationInterface
{
    /**
     *
     * @return \Grishalive\ClickhouseMigrations\Builder
     */
    protected function getBuilder(): \Grishalive\ClickhouseMigrations\Builder
    {
        return \Grishalive\ClickhouseMigrations\Clickhouse::builder();
    }

}

<?php

namespace Grishalive\ClickhouseMigrations;

use Tinderbox\ClickhouseBuilder\Exceptions\GrammarException;
use Tinderbox\ClickhouseBuilder\Query\Enums\Format;
use Tinderbox\ClickhouseBuilder\Query\Expression;
use Tinderbox\ClickhouseBuilder\Query\Identifier;
use Tinderbox\ClickhouseBuilder\Query\Traits\ArrayJoinComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\ColumnsComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\FormatComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\FromComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\GroupsComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\HavingsComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\JoinComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\LimitByComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\LimitComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\OrdersComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\PreWheresComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\SampleComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\TupleCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\TwoElementsLogicExpressionsCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\UnionsComponentCompiler;
use Tinderbox\ClickhouseBuilder\Query\Traits\WheresComponentCompiler;

class Grammar extends \Tinderbox\ClickhouseBuilder\Query\Grammar
{

    /**
     * Compile insert query for values.
     *
     * @param BaseBuilder $query
     * @param             $values
     *
     * @throws GrammarException
     *
     * @return string
     */
    public function compileBatchInsert(Builder $query, $values) : string
    {
        $result = [];

        $from = $query->getFrom();

        if (is_null($from)) {
            throw GrammarException::missedTableForInsert();
        }

        $table = $this->wrap($from->getTable());

        if (is_null($table)) {
            throw GrammarException::missedTableForInsert();
        }

        $format = $query->getFormat() ?? Format::VALUES;

        if ($format == Format::VALUES) {
            $columns = array_map(function ($col) {
                return is_string($col) ? new \Tinderbox\ClickhouseBuilder\Query\Identifier($col) : null;
            }, array_keys($values[0]));

            $columns = array_filter($columns);
        }

        $columns = $this->compileTuple(new \Tinderbox\ClickhouseBuilder\Query\Tuple($columns));

        $result[] = "INSERT INTO {$table}";

        if ($columns !== '') {
            $result[] = "({$columns})";
        }

        $result[] = 'FORMAT ' . $format;

        if ($format == Format::VALUES) {
            $result[] = $this->compileInsertValues($values);
        }

        return implode(' ', $result);
    }

    /**
     * @param $values
     * @return string
     */
    public function compileInsertValues($values)
    {
        return implode(', ', array_map(function ($value) {
            return '(' . implode(', ', array_map(function ($value) {
                    $value = $this->wrap($value);
                    if (is_array($value)) {
                        $value = '[' . implode(',', $value) . ']';
                    }
                    return $value;
                }, $value)) . ')';
        }, $values));
    }

    /**
     * Convert value in literal.
     *
     * @param string|Expression|Identifier|array $value
     *
     * @return string|array|null|int
     */
    public function wrap($value)
    {
        if ($value instanceof Expression) {
            return $value->getValue();
        } elseif (is_array($value)) {
            return array_map([$this, 'wrap'], $value);
        } elseif (is_string($value)) {
            $value = addslashes($value);
            return "'{$value}'";
        } elseif ($value instanceof Identifier) {
            $value = (string)$value;

            if (strpos(strtolower($value), '.') !== false) {
                return implode('.', array_map(function ($element) {
                    return $this->wrap(new Identifier($element));
                }, array_map('trim', preg_split('/\./', $value))));
            }

            if (strpos(strtolower($value), ' as ') !== false) {
                list($value, $alias) = array_map('trim', preg_split('/\s+as\s+/i', $value));

                $value = $this->wrap(new Identifier($value));
                $alias = $this->wrap(new Identifier($alias));

                $value = "$value AS $alias";

                return $value;
            }

            if ($value === '*') {
                return $value;
            }

            return '`' . str_replace('`', '``', $value) . '`';
        } elseif (is_numeric($value)) {
            return $value;
        } else {
            $stringValue = strval($value);
            return "'{$stringValue}'";
        }
    }
}

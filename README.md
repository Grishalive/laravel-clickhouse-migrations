# Clickhouse DB migrations for Laravel, Lumen

ClickHouse is an open source column-oriented database management system capable of real time generation of analytical data reports using SQL queries.
Library is suitable for Laravel, Lumen.

## Installing

```bash
composer require grishalive/laravel-clickhouse-migrations
```

## Usage

### Configure /config/database.php

Example for clickhouse and migrations settings:

```php
...
'connections' => [
    'clickhouse' => [
        'host' => env('CLICKHOUSE_HOST', 'localhost'),
        'port' => env('CLICKHOUSE_PORT', 8123),
        'username' => env('CLICKHOUSE_USER', 'default'),
        'password' => env('CLICKHOUSE_PASSWORD', ''),
        'options' => [
            'database' => env('CLICKHOUSE_DATABASE', 'default'),
            'timeout' => 1,
            'connectTimeOut' => 2,
        ],
    ],
],
'clickhouse-migrations' => [
    'dir' => env('CLICKHOUSE_MIGRATION_DIR', '/database/clickhouse-migrations/'),
    'table' => env('CLICKHOUSE_MIGRATION_TABLE_NAME', 'migrations'),
],
    ...
```

### Register provider

```php
'providers' => [
    ...
    \Grishalive\ClickhouseMigrations\ClickhouseProvider::class,
    \Tinderbox\ClickhouseBuilder\Integrations\Laravel\ClickhouseServiceProvider::class;
    ...
],
```

## Usage

### Create new migration

```bash
php artisan clickhouse:migration:create {name}
```

### Up migrations

```bash
php artisan clickhouse:migrate
```

### Down last migration

```
php artisan clickhouse:migrate --down
```

## Built With

* https://github.com/the-tinderbox/ClickhouseBuilder - Fluent queries builder for Clickhouse.
